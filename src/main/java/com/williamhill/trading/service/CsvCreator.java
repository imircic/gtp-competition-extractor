package com.williamhill.trading.service;

import com.williamhill.trading.api.GtpCompetition;
import com.williamhill.trading.api.GtpCompetitionList;
import com.williamhill.trading.api.WhExcelCompetition;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@Slf4j
public class CsvCreator {
    private static final String CSV_SEPARATOR = ",";
    private static final String METDATA_SEPARATOR = ";";
    private static final String TEST_DATA_PREFIX = "Test_";

    private static final String COMPETITION_TYPE = "competition_type=";
    private static final String COMPETITION_TYPE_DEFAULT = "League";
    private static final String TIER = "tier=";
    private static final String TIER_DEFAULT = "1";
    private static final String SEX = "sex=";
    private static final String SEX_DEFAULT = "Men's";
    private static final String CLUB_INTERNATIONAL = "club_international=";
    private static final String CLUB_INTERNATIONAL_DEFAULT = "Club";
    private static final String CITY = "city=";
    private static final String FORMAT = "format=";
    private static final String TOUR = "tour=";
    private static final String PM_USE_AUTOMATION = "PM_USE_AUTOMATION=";
    private static final String PM_USE_AUTOMATION_DEFAULT = "TRUE";
    private static final String PM_USE_AUTO_1X2 = "PM_USE_AUTO_1X2=";
    private static final String PM_USE_AUTO_1X2_DEFAULT = "TRUE";
    private static final String PM_MARKET_LEVEL = "PM_MARKET_LEVEL=";
    private static final String PM_MARKET_LEVEL_DEFAULT = "FULL";
    private static final String IP_MARKET_LEVEL = "IP_MARKET_LEVEL=";
    private static final String IP_MARKET_LEVEL_DEFAULT = "FULL";
    private static final String INCIDENT_FEED_REQ = "INCIDENT_FEED_REQ=";
    private static final String INCIDENT_FEED_REQ_DEFAULT = "TRUE";
    private static final String PRE_MATCH_TARGET_PERCENTAGE = "MATCH_BETTING.preMatchTargetPercentage=";
    private static final String PRE_MATCH_TARGET_PERCENTAGE_DEFAULT = "112.4";
    private static final String IN_PLAY_TARGET_PERCENTAGE = "MATCH_BETTING.inPlayTargetPercentage=";
    private static final String IN_PLAY_TARGET_PERCENTAGE_DEFAULT = "112.4";
    private static final String PRE_MATCH_TARGET_PERCENTAGE_SSBT = "MATCH_BETTING_SSBT.preMatchTargetPercentage=";
    private static final String PRE_MATCH_TARGET_PERCENTAGE_SSBT_DEFAULT = "112.4";
    private static final String IN_PLAY_TARGET_PERCENTAGE_SSBT = "MATCH_BETTING_SSBT.inPlayTargetPercentage=";
    private static final String IN_PLAY_TARGET_PERCENTAGE_SSBT_DEFAULT = "112.4";
    public static final String MATCH_BETTING = "MATCH_BETTING";
    public static final String MATCH_BETTING_SSBT = "MATCH_BETTING_SSBT";
    public static final String PRE_MATCH = "PRE_MATCH";
    public static final String IN_PLAY = "IN_PLAY";

    @Value("${gtp.whCategory.id}")
    private String gtpWhCategoryId;
    @Value("${gtp.whCategory.default.level}")
    private String defaultLevel;
    @Value("${gtp.whCategory.default.metadata}")
    private String defaultMetadata;
    @Value("${gtp.whCategory.default.modelVariables}")
    private String defaultModelVariables;
    @Value("${nameOfCsvFileToCreate}")
    private String nameOfCsvFileToCreate;
    @Value("${nameOfCsvFileToUpdate}")
    private String nameOfCsvFileToUpdate;
    @Value("${gtp.db-dump.selectedcategory}")
    private String gtpDbDumpSelectedcategory;

    @Autowired
    private WhExcelCompetitionsReader whExcelCompetitionsReader;

    private Map<String, WhExcelCompetition> notProcessedWhCompetitions;

    public void create(GtpCompetitionList gtpCompetitionList) throws Exception {
        final OutputStream notExistInGTPCompetitions = new FileOutputStream(nameOfCsvFileToCreate);
        final OutputStream alreadyExistInGTPCompetitions = new FileOutputStream(nameOfCsvFileToUpdate);

        final Map<String, WhExcelCompetition> whExcelCompetitionMap = whExcelCompetitionsReader.getWhExcelCompetitions();
        //for statistics
        log.info("Number of Gtp Competition to process : " + gtpCompetitionList.size());
        if (gtpDbDumpSelectedcategory.equals("football")) {
            log.info("Number of Wh Competition to process : " + whExcelCompetitionMap.size());
        }

        notProcessedWhCompetitions = new HashMap<>();
        notProcessedWhCompetitions.putAll(whExcelCompetitionMap);
        AtomicInteger allGTPCompetitions = new AtomicInteger(0);
        AtomicInteger halfProcessedCompetitions = new AtomicInteger(0);
        AtomicInteger fullProcessedCompetitions = new AtomicInteger(0);

        try (
                Writer alreadyExistInGTPCompetitionsOutWriter = new OutputStreamWriter(alreadyExistInGTPCompetitions, "UTF-8");
                BufferedWriter alreadyExistInGTPCompetitionsOut = new BufferedWriter(alreadyExistInGTPCompetitionsOutWriter);

                Writer notExistInGTPCompetitionsOutWriter = new OutputStreamWriter(notExistInGTPCompetitions, "UTF-8");
                BufferedWriter notExistInGTPCompetitionsOut = new BufferedWriter(notExistInGTPCompetitionsOutWriter);
        ) {
            log.warn("List of GTP Competitions which are partially or fully mapped:");
            gtpCompetitionList.stream()
                    .filter(gtpCompetition ->
                            (gtpCompetition.getData().getName() != null &&
                                    !gtpCompetition.getData().getName().contains(TEST_DATA_PREFIX)) ||
                                    (gtpCompetition.getData().getOfficialName() != null &&
                                            !gtpCompetition.getData().getOfficialName().contains(TEST_DATA_PREFIX))
                    )
                    .map(gtpCompetition -> {
                        String id = gtpCompetition.getCompetitionId();
                        String name = gtpCompetition.getData().getOfficialName() != null ? gtpCompetition.getData().getOfficialName() : gtpCompetition.getData().getName();
                        //escape , (some tennis competitions have , inside name)
                        name = name.replace("\\", "\\\\");
                        name = name.replace(CSV_SEPARATOR, "\\,");
                        WhExcelCompetition whExcelCompetition = whExcelCompetitionMap.get(name);
                        String country = gtpCompetition.getData().getCountry();
                        String type = null;
                        String tier = null;
                        String sex = null;
                        String clubInternational = null;
                        String preMatchAuto = null;
                        String preMatchAuto1x2 = null;
                        String preMatchLevel = null;
                        String feedRequired = null;
                        String minute90Market = null;
                        String minute90SSBT = null;
                        String matchBettingLive = null;
                        String matchBettingLiveSSBT = null;
                        String inPlayLevel = null;
                        String city = null;
                        String format = null;
                        String tour = null;
                        //football
                        if (gtpDbDumpSelectedcategory.equals("football") && whExcelCompetition != null) {
                            type = whExcelCompetition.getType() != null && !whExcelCompetition.getType().isEmpty() ? whExcelCompetition.getType() : COMPETITION_TYPE_DEFAULT;
                            tier = whExcelCompetition.getTier() != null && !whExcelCompetition.getTier().isEmpty() ? whExcelCompetition.getTier() : TIER_DEFAULT;
                            sex = whExcelCompetition.getSex() != null && !whExcelCompetition.getSex().isEmpty() ? whExcelCompetition.getSex() : SEX_DEFAULT;
                            clubInternational = whExcelCompetition.getClubInternational() != null && !whExcelCompetition.getClubInternational().isEmpty() ? whExcelCompetition.getClubInternational() : CLUB_INTERNATIONAL_DEFAULT;
                            preMatchAuto = whExcelCompetition.getPreMatchAuto() != null && !whExcelCompetition.getPreMatchAuto().isEmpty() ? whExcelCompetition.getPreMatchAuto() : PM_USE_AUTOMATION_DEFAULT;
                            preMatchAuto1x2 = whExcelCompetition.getPreMatchAuto1x2() != null && !whExcelCompetition.getPreMatchAuto1x2().isEmpty() ? whExcelCompetition.getPreMatchAuto1x2() : PM_USE_AUTO_1X2_DEFAULT;
                            preMatchLevel = whExcelCompetition.getPreMatchLevel() != null && !whExcelCompetition.getPreMatchLevel().isEmpty() ? whExcelCompetition.getPreMatchLevel() : PM_MARKET_LEVEL_DEFAULT;
                            feedRequired = whExcelCompetition.getFeedRequired() != null && !whExcelCompetition.getFeedRequired().isEmpty() ? whExcelCompetition.getFeedRequired() : INCIDENT_FEED_REQ_DEFAULT;
                            minute90Market = whExcelCompetition.getMinute90Market() != null && !whExcelCompetition.getMinute90Market().isEmpty() ? whExcelCompetition.getMinute90Market() : getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING, PRE_MATCH, PRE_MATCH_TARGET_PERCENTAGE_DEFAULT);
                            ;
                            minute90SSBT = whExcelCompetition.getMinute90SSBT() != null && !whExcelCompetition.getMinute90SSBT().isEmpty() ? whExcelCompetition.getMinute90SSBT() : getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING_SSBT, PRE_MATCH, PRE_MATCH_TARGET_PERCENTAGE_SSBT_DEFAULT);
                            matchBettingLive = whExcelCompetition.getMatchBettingLive() != null && !whExcelCompetition.getMatchBettingLive().isEmpty() ? whExcelCompetition.getMatchBettingLive() : getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING, IN_PLAY, IN_PLAY_TARGET_PERCENTAGE_DEFAULT);
                            matchBettingLiveSSBT = whExcelCompetition.getMatchBettingLiveSSBT() != null && !whExcelCompetition.getMatchBettingLiveSSBT().isEmpty() ? whExcelCompetition.getMatchBettingLiveSSBT() : getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING_SSBT, IN_PLAY, IN_PLAY_TARGET_PERCENTAGE_SSBT_DEFAULT);
                            inPlayLevel = whExcelCompetition.getInPlayLevel() != null && !whExcelCompetition.getInPlayLevel().isEmpty() ? whExcelCompetition.getInPlayLevel() : IP_MARKET_LEVEL_DEFAULT;
                            //add to list of processed wh competitions
                            notProcessedWhCompetitions.remove(whExcelCompetition.getName());
                            fullProcessedCompetitions.addAndGet(1);
                            log.info("{}. {} (Full Map)", allGTPCompetitions.addAndGet(1), name);
                        } else if (gtpDbDumpSelectedcategory.equals("football") && whExcelCompetition == null) {
                            type = COMPETITION_TYPE_DEFAULT;
                            tier = TIER_DEFAULT;
                            sex = SEX_DEFAULT;
                            clubInternational = CLUB_INTERNATIONAL_DEFAULT;
                            preMatchAuto = PM_USE_AUTOMATION_DEFAULT;
                            preMatchAuto1x2 = PM_USE_AUTO_1X2_DEFAULT;
                            preMatchLevel = PM_MARKET_LEVEL_DEFAULT;
                            feedRequired = INCIDENT_FEED_REQ_DEFAULT;
                            minute90Market = getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING, PRE_MATCH, PRE_MATCH_TARGET_PERCENTAGE_DEFAULT);
                            minute90SSBT = getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING_SSBT, PRE_MATCH, PRE_MATCH_TARGET_PERCENTAGE_SSBT_DEFAULT);
                            matchBettingLive = getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING, IN_PLAY, IN_PLAY_TARGET_PERCENTAGE_DEFAULT);
                            matchBettingLiveSSBT = getValueForGtpMarketPricing(gtpCompetition, MATCH_BETTING_SSBT, IN_PLAY, IN_PLAY_TARGET_PERCENTAGE_SSBT_DEFAULT);
                            inPlayLevel = IP_MARKET_LEVEL_DEFAULT;
                            halfProcessedCompetitions.addAndGet(1);
                            log.warn("{}. {} (GTP Only)", allGTPCompetitions.addAndGet(1), name);
                        }
                        if (gtpDbDumpSelectedcategory.equals("tennis")) {
                            //tennis
                            city = gtpCompetition.getData().getCity();
                            format = gtpCompetition.getData().getFormat();
                            tour = gtpCompetition.getData().getTour();
                        }
                        return id
                                + CSV_SEPARATOR + name
                                + CSV_SEPARATOR + gtpWhCategoryId
                                + CSV_SEPARATOR + country
                                + ((type != null) ? CSV_SEPARATOR + COMPETITION_TYPE + type : "")
                                + ((tier != null) ? METDATA_SEPARATOR + TIER + tier : "")
                                + ((sex != null) ? METDATA_SEPARATOR + SEX + sex : "")
                                + ((clubInternational != null) ? METDATA_SEPARATOR + CLUB_INTERNATIONAL + clubInternational : "")
                                + ((city != null) ? METDATA_SEPARATOR + CITY + city : "")
                                + ((format != null) ? METDATA_SEPARATOR + FORMAT + format : "")
                                + ((tour != null) ? METDATA_SEPARATOR + TOUR + tour : "")
                                + ((minute90Market != null) ? METDATA_SEPARATOR + PRE_MATCH_TARGET_PERCENTAGE + minute90Market : "")
                                + ((matchBettingLive != null) ? METDATA_SEPARATOR + IN_PLAY_TARGET_PERCENTAGE + matchBettingLive : "")
                                + ((minute90SSBT != null) ? METDATA_SEPARATOR + PRE_MATCH_TARGET_PERCENTAGE_SSBT + minute90SSBT : "")
                                + ((matchBettingLiveSSBT != null) ? METDATA_SEPARATOR + IN_PLAY_TARGET_PERCENTAGE_SSBT + matchBettingLiveSSBT : "")
                                + ((defaultMetadata != null && !defaultMetadata.isEmpty()) ? METDATA_SEPARATOR + defaultMetadata : "")
                                + ((preMatchAuto != null) ? CSV_SEPARATOR + PM_USE_AUTOMATION + preMatchAuto : "")
                                + ((preMatchAuto1x2 != null) ? METDATA_SEPARATOR + PM_USE_AUTO_1X2 + preMatchAuto1x2 : "")
                                + ((preMatchLevel != null) ? METDATA_SEPARATOR + PM_MARKET_LEVEL + preMatchLevel : "")
                                + ((inPlayLevel != null) ? METDATA_SEPARATOR + IP_MARKET_LEVEL + inPlayLevel : "")
                                + ((feedRequired != null) ? METDATA_SEPARATOR + INCIDENT_FEED_REQ + feedRequired : "")
                                + ((defaultModelVariables != null && !defaultModelVariables.isEmpty()) ? METDATA_SEPARATOR + defaultModelVariables : "");
                    })
                    .forEach(line -> {
                        try {
                            alreadyExistInGTPCompetitionsOut.write((String) line);
                            alreadyExistInGTPCompetitionsOut.write("\n");
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    });
            log.warn("List of Competitions for creation in GTP:");
            AtomicInteger numberOfUnprocessedWhCompetition = new AtomicInteger(0);
            if (gtpDbDumpSelectedcategory.equals("football")) {
                notProcessedWhCompetitions.values().stream().map(whExcelCompetition -> {
                    log.warn("{}. {} (WH Only)", numberOfUnprocessedWhCompetition.getAndAdd(1), whExcelCompetition.getName());

                    String id = UUID.randomUUID().toString();
                    String name = whExcelCompetition.getName();
                    String country = whExcelCompetition.getCountry();
                    String type = whExcelCompetition.getType() != null && !whExcelCompetition.getType().isEmpty() ? whExcelCompetition.getType() : COMPETITION_TYPE_DEFAULT;
                    String tier = whExcelCompetition.getTier() != null && !whExcelCompetition.getTier().isEmpty() ? whExcelCompetition.getTier() : TIER_DEFAULT;
                    String sex = whExcelCompetition.getSex() != null && !whExcelCompetition.getSex().isEmpty() ? whExcelCompetition.getSex() : SEX_DEFAULT;
                    String clubInternational = whExcelCompetition.getClubInternational() != null && !whExcelCompetition.getClubInternational().isEmpty() ? whExcelCompetition.getClubInternational() : CLUB_INTERNATIONAL_DEFAULT;
                    String preMatchAuto = whExcelCompetition.getPreMatchAuto() != null && !whExcelCompetition.getPreMatchAuto().isEmpty() ? whExcelCompetition.getPreMatchAuto() : PM_USE_AUTOMATION_DEFAULT;
                    String preMatchAuto1x2 = whExcelCompetition.getPreMatchAuto1x2() != null && !whExcelCompetition.getPreMatchAuto1x2().isEmpty() ? whExcelCompetition.getPreMatchAuto1x2() : PM_USE_AUTO_1X2_DEFAULT;
                    String preMatchLevel = whExcelCompetition.getPreMatchLevel() != null && !whExcelCompetition.getPreMatchLevel().isEmpty() ? whExcelCompetition.getPreMatchLevel() : PM_MARKET_LEVEL_DEFAULT;
                    String feedRequired = whExcelCompetition.getFeedRequired() != null && !whExcelCompetition.getFeedRequired().isEmpty() ? whExcelCompetition.getFeedRequired() : INCIDENT_FEED_REQ_DEFAULT;
                    String minute90Market = whExcelCompetition.getMinute90Market() != null && !whExcelCompetition.getMinute90Market().isEmpty() ? whExcelCompetition.getMinute90Market() : PRE_MATCH_TARGET_PERCENTAGE_DEFAULT;
                    String minute90SSBT = whExcelCompetition.getMinute90SSBT() != null && !whExcelCompetition.getMinute90SSBT().isEmpty() ? whExcelCompetition.getMinute90SSBT() : PRE_MATCH_TARGET_PERCENTAGE_SSBT_DEFAULT;
                    String matchBettingLive = whExcelCompetition.getMatchBettingLive() != null && !whExcelCompetition.getMatchBettingLive().isEmpty() ? whExcelCompetition.getMatchBettingLive() : IN_PLAY_TARGET_PERCENTAGE;
                    String matchBettingLiveSSBT = whExcelCompetition.getMatchBettingLiveSSBT() != null && !whExcelCompetition.getMatchBettingLiveSSBT().isEmpty() ? whExcelCompetition.getMatchBettingLiveSSBT() : IN_PLAY_TARGET_PERCENTAGE_SSBT_DEFAULT;
                    String inPlayLevel = whExcelCompetition.getInPlayLevel() != null && !whExcelCompetition.getInPlayLevel().isEmpty() ? whExcelCompetition.getInPlayLevel() : IP_MARKET_LEVEL_DEFAULT;
                    String city = null;
                    String format = null;
                    String tour = null;

                    return id
                            + CSV_SEPARATOR + name
                            + CSV_SEPARATOR + gtpWhCategoryId
                            + CSV_SEPARATOR + country
                            + ((type != null) ? CSV_SEPARATOR + COMPETITION_TYPE + type : "")
                            + ((tier != null) ? METDATA_SEPARATOR + TIER + tier : "")
                            + ((sex != null) ? METDATA_SEPARATOR + SEX + sex : "")
                            + ((clubInternational != null) ? METDATA_SEPARATOR + CLUB_INTERNATIONAL + clubInternational : "")
                            + ((city != null) ? METDATA_SEPARATOR + CITY + city : "")
                            + ((format != null) ? METDATA_SEPARATOR + FORMAT + format : "")
                            + ((tour != null) ? METDATA_SEPARATOR + TOUR + tour : "")
                            + ((minute90Market != null) ? METDATA_SEPARATOR + PRE_MATCH_TARGET_PERCENTAGE + minute90Market : "")
                            + ((matchBettingLive != null) ? METDATA_SEPARATOR + IN_PLAY_TARGET_PERCENTAGE + matchBettingLive : "")
                            + ((minute90SSBT != null) ? METDATA_SEPARATOR + PRE_MATCH_TARGET_PERCENTAGE_SSBT + minute90SSBT : "")
                            + ((matchBettingLiveSSBT != null) ? METDATA_SEPARATOR + IN_PLAY_TARGET_PERCENTAGE_SSBT + matchBettingLiveSSBT : "")
                            + ((defaultMetadata != null && !defaultMetadata.isEmpty()) ? METDATA_SEPARATOR + defaultMetadata : "")
                            + ((preMatchAuto != null) ? CSV_SEPARATOR + PM_USE_AUTOMATION + preMatchAuto : "")
                            + ((preMatchAuto1x2 != null) ? METDATA_SEPARATOR + PM_USE_AUTO_1X2 + preMatchAuto1x2 : "")
                            + ((preMatchLevel != null) ? METDATA_SEPARATOR + PM_MARKET_LEVEL + preMatchLevel : "")
                            + ((inPlayLevel != null) ? METDATA_SEPARATOR + IP_MARKET_LEVEL + inPlayLevel : "")
                            + ((feedRequired != null) ? METDATA_SEPARATOR + INCIDENT_FEED_REQ + feedRequired : "")
                            + ((defaultModelVariables != null && !defaultModelVariables.isEmpty()) ? METDATA_SEPARATOR + defaultModelVariables : "");
                }).forEach(line -> {
                    try {
                        notExistInGTPCompetitionsOut.write((String) line);
                        notExistInGTPCompetitionsOut.write("\n");
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                });
            }

            if (gtpDbDumpSelectedcategory.equals("football")) {
                log.info("Number of fully processed competitions (both GTP and WH) is {}", fullProcessedCompetitions.get());
                log.info("Number of competitions for creation (only WH) is {}", numberOfUnprocessedWhCompetition.get());
            }
            log.info("Number of half processed competitions (only GTP) is {}", halfProcessedCompetitions.get());
        }
    }

    private String getValueForGtpMarketPricing(GtpCompetition gtpCompetition, String group, String phase, String defaultValue) {
        if (gtpCompetition.getPricingConfig() != null &&
                gtpCompetition.getPricingConfig().getMarketPricingGroups() != null &&
                gtpCompetition.getPricingConfig().getMarketPricingGroups().get(group) != null &&
                gtpCompetition.getPricingConfig().getMarketPricingGroups().get(group).getPhases() != null &&
                gtpCompetition.getPricingConfig().getMarketPricingGroups().get(group).getPhases().get(phase) != null &&
                gtpCompetition.getPricingConfig().getMarketPricingGroups().get(group).getPhases().get(phase).getTargetBookPercentage() != null &&
                gtpCompetition.getPricingConfig().getMarketPricingGroups().get(group).getPhases().get(phase).getTargetBookPercentage().getValue() != null) {
            return gtpCompetition.getPricingConfig().getMarketPricingGroups().get(group).getPhases().get(phase).getTargetBookPercentage().getValue();
        } else {
            return defaultValue;
        }
    }
}
