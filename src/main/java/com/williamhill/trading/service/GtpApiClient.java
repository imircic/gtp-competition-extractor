package com.williamhill.trading.service;

import com.williamhill.trading.api.GtpCompetitionList;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class GtpApiClient {

    @Value("${gtp.getcompetitions.address}")
    private String gtpGetcompetitionsAddress;

    @Autowired
    private RestTemplate restTemplate;

    public GtpCompetitionList getCompetitions() {
        log.info("requesting GTP competitions");
        GtpCompetitionList result = null;
        ResponseEntity<GtpCompetitionList> responseEntity = restTemplate.getForEntity(gtpGetcompetitionsAddress, GtpCompetitionList.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            result = responseEntity.getBody();
        } else {
            log.error("Can not execute GET request to address: " + gtpGetcompetitionsAddress);
        }
        return result;
    }

}
