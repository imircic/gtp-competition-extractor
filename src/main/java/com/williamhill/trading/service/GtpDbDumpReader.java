package com.williamhill.trading.service;

import com.williamhill.trading.api.GtpCompetition;
import com.williamhill.trading.api.GtpCompetitionList;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

@Service
@Slf4j
public class GtpDbDumpReader {

    @Value("${gtp.db-dump.path}")
    private String gtpDbDumpPath;
    @Value("${gtp.db-dump.selectedcategory}")
    private String gtpDbDumpSelectedcategory;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Jackson2JsonObjectMapper jsonObjectMapper;

    public GtpCompetitionList getCompetitions() throws Exception {
        log.info("Start to read GTP DB dump files");
        GtpCompetitionList result = new GtpCompetitionList();

        TarArchiveInputStream tarInput = new TarArchiveInputStream(new GzipCompressorInputStream(new FileInputStream(gtpDbDumpPath)));
        TarArchiveEntry currentEntry = tarInput.getNextTarEntry();

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        while (currentEntry != null) {
            br = new BufferedReader(new InputStreamReader(tarInput)); // Read directly from tarInput
            System.out.println("For File = " + currentEntry.getName());
            String line;
            while ((line = br.readLine()) != null) {
                log.info("Gtp competition: {}", line);
                try {
                    GtpCompetition gtpCompetition = jsonObjectMapper.fromJson(line, GtpCompetition.class);
                    if (gtpCompetition.getCategoryName().equals(gtpDbDumpSelectedcategory)) {
                        result.add(gtpCompetition);
                    }
                } catch (Exception exp) {
                    log.warn("Not possible to deserialize");
                }
            }
            currentEntry = tarInput.getNextTarEntry(); // You forgot to iterate to the next file
        }

        log.info("Read {} competitions from GTP DB dump.", result.size());

        return result;
    }

}
