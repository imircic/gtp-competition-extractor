package com.williamhill.trading.service;

import com.williamhill.trading.api.WhExcelCompetition;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
@Slf4j
public class WhExcelCompetitionsReader {
    @Value("${gtp.excel.path}")
    private String whExcelCompetitionsFilePath;
    @Value("${gtp.db-dump.selectedcategory}")
    private String gtpDbDumpSelectedcategory;

    public Map<String, WhExcelCompetition> getWhExcelCompetitions() throws Exception {
        log.info("Start to read xlsx file and populate set of all competitions");
        Map<String, WhExcelCompetition> whExcelCompetitions = new HashMap<>();

        //this file is only use for competitions
        if (!gtpDbDumpSelectedcategory.equals("football")) {
            return whExcelCompetitions;
        }

        File whExcelCompetitionsFile = new File(whExcelCompetitionsFilePath);
        FileInputStream fis = new FileInputStream(whExcelCompetitionsFile);

        // Find the workbook instance for XLSX file
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        //get proper sheet
        XSSFSheet sheet = workbook.getSheetAt(0);
        //read through rows
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            //read through all columns for every row
            if (!toProcess(row)) {
                continue;
            }
            //create whExcelCompetition
            WhExcelCompetition whExcelCompetition = new WhExcelCompetition();
            whExcelCompetition.setName(row.getCell(1).getStringCellValue());
            whExcelCompetition.setCountry(row.getCell(3) != null && row.getCell(3).getCellTypeEnum().equals(CellType.STRING) ? row.getCell(3).getStringCellValue() : null);
            whExcelCompetition.setType(row.getCell(4) != null && row.getCell(4).getCellTypeEnum().equals(CellType.STRING) ? row.getCell(4).getStringCellValue() : null);
            whExcelCompetition.setTier(row.getCell(5) != null && row.getCell(5).getCellTypeEnum().equals(CellType.NUMERIC) ? String.valueOf(Double.valueOf(row.getCell(5).getNumericCellValue()).intValue()) : (row.getCell(5) != null && row.getCell(5).getCellTypeEnum().equals(CellType.STRING) ? row.getCell(5).getStringCellValue() : null));
            whExcelCompetition.setSex(row.getCell(6) != null && row.getCell(6).getCellTypeEnum().equals(CellType.STRING) ? row.getCell(6).getStringCellValue() : null);
            whExcelCompetition.setClubInternational(row.getCell(7) != null && row.getCell(7).getCellTypeEnum().equals(CellType.STRING) ? row.getCell(7).getStringCellValue() : null);
            whExcelCompetition.setFeedRequired(row.getCell(8) != null && row.getCell(8).getCellTypeEnum().equals(CellType.STRING) && row.getCell(8).getStringCellValue().equals("Yes") ? "TRUE" : "FALSE");
            whExcelCompetition.setPreMatchAuto(row.getCell(9) != null && row.getCell(9).getCellTypeEnum().equals(CellType.STRING) && row.getCell(9).getStringCellValue().equals("Yes") ? "TRUE" : "FALSE");
            whExcelCompetition.setPreMatchAuto1x2(row.getCell(10) != null && row.getCell(10).getCellTypeEnum().equals(CellType.STRING) && row.getCell(10).getStringCellValue().equals("Yes") ? "TRUE" : "FALSE");
            whExcelCompetition.setPreMatchLevel(row.getCell(11) != null && row.getCell(11).getCellTypeEnum().equals(CellType.STRING) && !row.getCell(11).getStringCellValue().equals("No markets") ? row.getCell(11).getStringCellValue() : null);
            whExcelCompetition.setMinute90Market(row.getCell(12) != null && row.getCell(12).getCellTypeEnum().equals(CellType.NUMERIC) ? BigDecimal.valueOf(row.getCell(12).getNumericCellValue()).toPlainString() : null);
            whExcelCompetition.setMinute90SSBT(row.getCell(13) != null && row.getCell(13).getCellTypeEnum().equals(CellType.NUMERIC) ? BigDecimal.valueOf(row.getCell(13).getNumericCellValue()).toPlainString() : null);
            whExcelCompetition.setMatchBettingLive(row.getCell(14) != null && row.getCell(14).getCellTypeEnum().equals(CellType.NUMERIC) ? BigDecimal.valueOf(row.getCell(14).getNumericCellValue()).toPlainString() : null);
            whExcelCompetition.setMatchBettingLiveSSBT(row.getCell(15) != null && row.getCell(15).getCellTypeEnum().equals(CellType.NUMERIC) ? BigDecimal.valueOf(row.getCell(15).getNumericCellValue()).toPlainString() : null);
            whExcelCompetition.setInPlayLevel(row.getCell(16) != null && row.getCell(16).getCellTypeEnum().equals(CellType.STRING) && !row.getCell(16).getStringCellValue().equals("No markets") ? row.getCell(16).getStringCellValue() : null);

            //by putting in list with key teamId we make sure that duplications will never happen
            whExcelCompetitions.put(whExcelCompetition.getName(), whExcelCompetition);
        }

        log.info("Read {} competitions from excel file.", whExcelCompetitions.size());

        return whExcelCompetitions;
    }

    private boolean toProcess(Row row) {
        if (row.getRowNum() == 0) {
            //this is first row ignore it
            return false;
        }

        if (row.getCell(1) == null || row.getCell(1).getCellTypeEnum().equals(CellType.BLANK)) {
            //if competition do not have name ignore it
            return false;
        }

        for (int i = 1; i <= 16; i++) {
            Cell cell = row.getCell(i);
            if (cell == null) {
                continue;
            } else if (cell.getCellTypeEnum().equals(CellType.STRING) && cell.getStringCellValue().contains("DUPLICATES")) {
                return false;
            } else if (cell.getCellTypeEnum().equals(CellType.STRING) && cell.getStringCellValue().contains("Duplicate")) {
                return false;
            } else if (cell.getCellTypeEnum().equals(CellType.STRING) && cell.getStringCellValue().contains("suspended")) {
                return false;
            } else if (cell.getCellTypeEnum().equals(CellType.STRING) && cell.getStringCellValue().contains("No markets")) {
                return false;
            }
        }

        return true;
    }
}
