package com.williamhill.trading.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.williamhill.trading.api.GtpCompetitionList;
import com.williamhill.trading.service.CsvCreator;
import com.williamhill.trading.service.GtpApiClient;
import com.williamhill.trading.service.GtpDbDumpReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.endpoint.MethodInvokingMessageSource;
import org.springframework.integration.json.JsonToObjectTransformer;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.web.client.RestTemplate;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration
public class SpringIntegrationConfig {

    @Bean
    public RestTemplate restTemplate() {
        return  new RestTemplate();
    }

    @Bean
    public GtpApiClient gtpApiClient() {
        return new GtpApiClient();
    }

    @Bean
    public GtpDbDumpReader gtpDbDumpReader() {
        return new GtpDbDumpReader();
    }

    @Bean
    public CsvCreator csvCreator() {
        return new CsvCreator();
    }

    @Profile("fromRest")
    @Bean(name = "gtpCompetitionsSource")
    public MessageSource<?> gtpCompetitionsRestSource(GtpApiClient gtpApiClient) {
        MethodInvokingMessageSource source = new MethodInvokingMessageSource();
        source.setObject(gtpApiClient);
        source.setMethodName("getCompetitions");
        return source;
    }

    @Profile("fromFile")
    @Bean(name = "gtpCompetitionsSource")
    public MessageSource<?> gtpCompetitionsFileSource(GtpDbDumpReader gtpDbDumpReader) {
        MethodInvokingMessageSource source = new MethodInvokingMessageSource();
        source.setObject(gtpDbDumpReader);
        source.setMethodName("getCompetitions");
        return source;
    }

    @Bean
    public JsonToObjectTransformer jsonToGtpCompetitionTransformer(Jackson2JsonObjectMapper jsonObjectMapper) {
        return new JsonToObjectTransformer(GtpCompetitionList.class, jsonObjectMapper);
    }

    @Bean
    public Jackson2JsonObjectMapper jsonObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        Jackson2JsonObjectMapper jackson2JsonObjectMapper  = new Jackson2JsonObjectMapper(objectMapper);
        return jackson2JsonObjectMapper;
    }

    @Bean
    public IntegrationFlow gtpCompetitionExtractionFlow(MessageSource<?> gtpCompetitionsSource, CsvCreator csvCreator) {
        return IntegrationFlows.from(gtpCompetitionsSource, c -> c.poller(Pollers.fixedDelay(1, TimeUnit.HOURS)))
                .handle(csvCreator, "create")
                .get();
    }
}
