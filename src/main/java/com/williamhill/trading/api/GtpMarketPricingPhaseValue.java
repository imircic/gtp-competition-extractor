package com.williamhill.trading.api;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class GtpMarketPricingPhaseValue {
    private String value;
}
