package com.williamhill.trading.api;

import lombok.Data;

import java.util.Map;

@Data
public class GtpCompetition {
    private GtpCompetitionData data;
    private String categoryName;
    private String competitionId;
    private GtpPricingConfig pricingConfig;
    private Map pricing;
}
