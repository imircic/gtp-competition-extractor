package com.williamhill.trading.api;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class WhExcelCompetition {
    private String name;
    private String country;
    private String type;
    private String tier;
    private String sex;
    private String clubInternational;
    private String feedRequired;
    private String preMatchAuto;
    private String preMatchAuto1x2;
    private String preMatchLevel;
    private String minute90Market;
    private String minute90SSBT;
    private String matchBettingLive;
    private String matchBettingLiveSSBT;
    private String inPlayLevel;
}
