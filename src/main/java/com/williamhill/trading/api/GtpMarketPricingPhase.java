package com.williamhill.trading.api;

import lombok.Data;

@Data
public class GtpMarketPricingPhase {
    private String applies;
    private GtpMarketPricingPhaseValue targetBookPercentage;
}
