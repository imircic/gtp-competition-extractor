package com.williamhill.trading.api;

import lombok.Data;

import java.util.Map;

@Data
public class GtpPricingConfig {
    private Map<String, GtpMarketPricingGroup> marketPricingGroups;
}
