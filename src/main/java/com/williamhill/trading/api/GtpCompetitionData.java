package com.williamhill.trading.api;

import lombok.Data;

@Data
public class GtpCompetitionData {
    private String country;
    private String name;
    private String city;
    private String format;
    private String tour;
    private String officialName;
}
