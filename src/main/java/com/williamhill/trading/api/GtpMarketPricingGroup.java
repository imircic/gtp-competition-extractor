package com.williamhill.trading.api;

import lombok.Data;

import java.util.Map;

@Data
public class GtpMarketPricingGroup {
    private String marketPricingGroupName;
    private Map<String, GtpMarketPricingPhase> phases;
}
